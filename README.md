# wp-pre-commit-hook
Pre-commit hook for WordPress projects.

This package will install PHPCS with the WordPress Coding Standard definitions together with a git pre-commit hook script that will run PHP linting and the PHPCS whenever you do a commit to your project.

If your project has a `phpcs.ruleset.xml` or `phpcs.xml.dist` file available, the pre-commit hook will use this file as a ruleset definition. Otherwise the “WordPress” ruleset will be used.

## Installation

`composer config repositories.wp-pre-commit-hook git git@bitbucket.org:acato/wp-pre-commit-hook.git`
`composer require acato/wp-pre-commit-hook --dev`

Or manually add to your `composer.json`;

```
    "config" : {
        "allow-plugins" : {
            "composer/installers" : true,
            "dealerdirect/phpcodesniffer-composer-installer" : true,
            "acato/wp-pre-commit-hook" : true
        }
    },
    "repositories" : [
        {
            "type" : "composer",
            "url" : "https://packagist.acato.nl"
        }
    ],
    "require-dev": {
        "acato/wp-pre-commit-hook": "*",
        "composer/installers": "~1.0"
    },
```

Then do a `composer update`

## Credits
This plugin has been based on https://github.com/bjornjohansen/wp-pre-commit-hook version 0.3.0 and has been adapted to support composer 2.0 and Windows with Git Bash.
